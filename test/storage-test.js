const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Storage", function () {
  it("delegatecall test", async function () {
   const StorageContract = await ethers.getContractFactory("StorageContract");
   const storage = await StorageContract.deploy();
   await storage.deployed();

   const StorageImplementation = await ethers.getContractFactory("StorageImplementation");
   const storageImpl = await StorageImplementation.deploy();
   await storageImpl.deployed();

   storage.setImplementation(storageImpl.address);

   console.log("after test");

   let impl = await storage.getImplementation();
   console.log("impl:" + impl);


    let helloResp = await storage.hello();

//    assert.equal(res, 5, "result shold be 5");
    expect(helloResp).to.equal("hello");
  });
});
