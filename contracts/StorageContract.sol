// SPDX-License-Identifier: MIT
pragma solidity 0.8.13;

import "hardhat/console.sol";

contract StorageContract {

    address private implementation;

    function setImplementation(address storageimplementation) external
    {
        implementation = storageimplementation;
    }

    function getImplementation() external view returns (address)
    {
        return implementation;
    }

//    function test(uint256 q, uint256 w) public
//    {
//        console.log("executing test");
//
//    (bool success, bytes memory returndata) = implementation.delegatecall(abi.encodeWithSignature("add(uint256,uint256)",q, w));
//
//        //        (bool success, bytes memory returndata) = address(tepStorage).call(abi.encodeWithSignature("addPendingIssuer(address,address)", msg.sender, pendingIssuer));
//    }


    fallback() external
    {
        console.log("executing fallback-------");
        delegate(implementation);
    }

    function delegate(address a) internal
    {
        assembly
        {
            calldatacopy(0, 0, calldatasize())

            let result := delegatecall(gas(), a, 0, calldatasize(), 0, 0)

            returndatacopy(0, 0, returndatasize())

            switch result
            case 0
            {
                revert(0, returndatasize())
            }
            default
            {
                return(0, returndatasize())
            }
        }
    }


}